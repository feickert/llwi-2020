# [Search for new heavy resonances in leptonic or hadronic final states with the ATLAS detector](https://indico.cern.ch/event/846070/contributions/3692950/)

Talk given at the [2020 Lake Louise Winter Institute](https://indico.cern.ch/event/846070/)

## Abstract

Many theories beyond the Standard Model (BSM) predict new phenomena at the highest energies accessible by the LHC.
Several searches for new heavy resonances have been performed by the ATLAS experiment, covering various BSM models.
This talk presents results on heavy resonances decaying to final states containing either isolated high-pt leptons or pairs of high-pt jets, either exclusively or in conjunction with other objects such as missing transverse energy or radiated photons or jets.
The talk will focus on the most recent full Run 2 results using 13 TeV _pp_ collision data.
Prospects for HL-LHC will also be shown.

## Building

To build this talk:

1. Install the included `Humor-Sans.ttf` font on your local machine.
2. Create a Python 3 virtual environment and activate it. Then install the build dependencies with
```
python -m pip install -r requirements.txt
```
3. Run `make`
