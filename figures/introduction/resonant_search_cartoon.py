#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt
import scipy.stats as stats


def background_model(x, norm, decay_rate):
    return norm * np.exp(-x / decay_rate)


def signal_model(x, norm, mean, width):
    return norm * stats.norm.pdf(x, mean, width)


def main():
    maximum_mass = 100
    invariant_mass = np.linspace(0, maximum_mass, 1000)

    # background
    decay_rate = maximum_mass * 0.3
    n_bkg = maximum_mass * 100000

    # signal
    n_signal = 0.7 * n_bkg
    signal_pole_mass = 0.2 * maximum_mass
    signal_width = signal_pole_mass * 0.15

    SM_bkg = background_model(invariant_mass, n_bkg, decay_rate)
    signal = signal_model(invariant_mass, n_signal, signal_pole_mass, signal_width)

    # Add noise
    np.random.seed(0)
    n_samples = 1000
    scatter_data = np.linspace(0, maximum_mass, int(n_samples / 40))
    observations = background_model(scatter_data, n_bkg, decay_rate) + signal_model(
        scatter_data, n_signal, signal_pole_mass, signal_width
    )
    observations = np.random.normal(
        observations, np.min(observations) / 2, len(scatter_data)
    )

    # Plotting
    plt.xkcd()
    fig, ax = plt.subplots()

    ax.plot(invariant_mass, SM_bkg, color="blue")
    ax.plot(invariant_mass, SM_bkg + signal, color="red", zorder=-1)

    ax.plot(scatter_data, observations, "+", color="black")

    ax.set_xlabel("mass", horizontalalignment="right", x=1.0)
    ax.set_ylabel("events", horizontalalignment="right", y=1.0)

    # Turn off axis ticks
    ax.xaxis.set_ticks_position("none")
    ax.yaxis.set_ticks_position("none")

    # Turn off top and right borders
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)

    # Turn off tick labels
    ax.set_yticklabels([])
    ax.set_xticklabels([])

    fig.savefig("resonant_search_cartoon.pdf")


if __name__ == "__main__":
    main()
