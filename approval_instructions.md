# ATLAS Internal Review Instructions

I will follow up the review and approval for your talk at LLWI2020,

Here are the steps you need to take:

1. Talks addressing ATLAS only results must be uploaded to CDS 10 days before the start of the conference, i.e. at the latest on 30th January 2020.
2. Please upload your talk on CDS as a COM note (choose "**Physics**" Category), and request approval of the corresponding COM note as a SLIDE.
It is this last step that will notify the Speakers Committee, who will then initiate the approval process of your draft talk.
Referees will be contacted and further instructions will be sent to you.
Note that, upon submission, if the CDS entry is of type COM-CONF instead of COM-PHYS, we will not receive your request approval and this will further delay the review of your slides.
3. After the upload please send me an email with the CDS information and with a clear subject title, e.g. _Matthew Feickert - LLWI2020 - Talk in CDS_.
This will help me to be right away aware that the submission has been done and I will invoke the ATLAS review in a timely manner.
At that moment you will also receive e-mails with the remaining information.
4. The rehearsal (Vidyo only meeting) of your talk will be scheduled.

Finally, let me remind you that the SC handles the review process of many posters and talks, so it is important that you follow the instructions to allow a timely review and approval of your talk.

Best regards,

Kostas Kordas

(for the ATLAS Speakers Committee)
